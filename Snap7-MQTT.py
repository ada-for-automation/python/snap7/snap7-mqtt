#!/usr/bin/env python3

"""
 This Python script implements a Python-Snap7 to MQTT Gateway

 Tutorial :
 http://www.steves-internet-guide.com/mqtt-python-beginners-course/
 
 References :
 https://pypi.org/project/paho-mqtt/
 
 https://pypi.org/project/python-snap7/

 https://python-snap7.readthedocs.io/en/latest/index.html

"""

import signal
import time
import os

import paho.mqtt.client as mqttc

import snap7

MQTT_Connection_Status = \
[
    "Connection successful",                              # 0
    "Connection refused - incorrect protocol version",    # 1
    "Connection refused - invalid client identifier",     # 2
    "Connection refused - server unavailable",            # 3
    "Connection refused - bad username or password",      # 4
    "Connection refused - not authorised",                # 5
    "Currently unused"                                    # 6 - 255
]

running = True

def quit():
    global running
    running = False
    print("Main : User interrupt !")

def sig_handler(signum, frame):
    quit()

def on_log(client, userdata, level, buf):
    print("MQTT Client log :", buf)

def on_connect(client, userdata, flags, rc):
    print("MQTT :", MQTT_Connection_Status[rc])
    
    if rc == 0 :
        print("MQTT : Subscribe to topics")
        client.subscribe(
        [
          ("cifX/Hello", 0),
          ("a4a/s7-315-2dp/mw0", 0)
        ])

        print("MQTT : Say hello !")
        client.publish("Python-Snap7/Hello", "True")

def on_disconnect(client, userdata, rc):
    if rc != 0 :
        print("MQTT : Unexpected disconnection.")
    else :
        print("MQTT : Disconnected.")

def on_message(client, userdata, message):
    print("MQTT : Got message :",
          "\n\tpayload\t\t",    str(message.payload.decode("utf-8")),
          "\n\ttopic\t\t",      message.topic,
          "\n\tqos\t\t",        message.qos,
          "\n\tretain flag\t",  message.retain)

    if message.topic == "a4a/s7-315-2dp/mw0" :
        userdata["A4A_MW0"] = int(str(message.payload.decode("utf-8")))
        print("MQTT : A4A_MW0 = ", userdata["A4A_MW0"])

def main():
    """
    Python-Snap7 to MQTT Gateway Main
    """

    print("=======================================\n"
          "  Python-Snap7 to MQTT Gateway Main !  \n"
          "=======================================\n")

    signal.signal(signal.SIGINT, sig_handler)

    PS7_Connect_Done = False
    PLC_IP_ADDR = os.environ.get("SNAP7_PLC_IP_ADDR", "192.168.253.240")
    PLC_RACK = int(os.environ.get("SNAP7_PLC_RACK", 0))
    PLC_SLOT = int(os.environ.get("SNAP7_PLC_SLOT", 3))
    PLC_PORT = 102

    snap7_client = snap7.client.Client()

    BROKER_HOST = os.environ.get("MQTT_BROKER_HOST", "localhost")

    MyData = {"A4A_MW0"     : 0
             }

    print("MQTT : Creating an instance of MQTT Client")
    mqtt_client = mqttc.Client(mqttc.CallbackAPIVersion.VERSION1,
                              "ps7", userdata = MyData)

    print("MQTT : Connecting handlers")
    mqtt_client.on_log = on_log
    mqtt_client.on_connect = on_connect
    mqtt_client.on_disconnect = on_disconnect
    mqtt_client.on_message = on_message

    print("MQTT : Connecting to broker :", BROKER_HOST)
    mqtt_client.connect(BROKER_HOST)

    print("MQTT : Starting loop")
    mqtt_client.loop_start()

    print("=========================================\n"
          "   IO Loop : Ctrl-c to terminate !\n"
          "=========================================\n")

    while running:

        if not PS7_Connect_Done:

            print("Main : Connection to PLC {ipaddr}/{rack}/{slot}"
                  .format(ipaddr = PLC_IP_ADDR,
                          rack = PLC_RACK, slot = PLC_SLOT))

            try:
                snap7_client.connect(PLC_IP_ADDR, PLC_RACK, PLC_SLOT, PLC_PORT)

            except RuntimeError as exc :
                print("Main : Error Connecting to PLC :\n", exc)

            else:
                PS7_Connect_Done = True

                cpu_info = snap7_client.get_cpu_info()
                print("Main : CPU Information:\n", cpu_info)

                mqtt_client.publish("ps7/s7-315-2dp/cpu_info/ModuleTypeName",
                                    cpu_info.ModuleTypeName.decode("ASCII"))

                mqtt_client.publish("ps7/s7-315-2dp/cpu_info/SerialNumber",
                                    cpu_info.SerialNumber.decode("ASCII"))

                mqtt_client.publish("ps7/s7-315-2dp/cpu_info/ASName",
                                    cpu_info.ASName.decode("ASCII"))

                mqtt_client.publish("ps7/s7-315-2dp/cpu_info/Copyright",
                                    cpu_info.Copyright.decode("ASCII"))

                mqtt_client.publish("ps7/s7-315-2dp/cpu_info/ModuleName",
                                    cpu_info.ModuleName.decode("ASCII"))

        else:

            while running and PS7_Connect_Done:

                try:
                    buffer = snap7_client.db_read(db_number = 1, start = 0,
                                                  size = 2)
                    db1_dbw0 = int.from_bytes(buffer[0:2],
                                              byteorder = 'big', signed = True)

                    buffer = snap7_client.eb_read(start = 0, size = 2)
                    ew0 = int.from_bytes(buffer[0:2],
                                         byteorder = 'big', signed = True)

                    buffer = snap7_client.ab_read(start = 0, size = 2)
                    aw0 = int.from_bytes(buffer[0:2],
                                         byteorder = 'big', signed = True)

                    buffer = snap7_client.mb_read(start = 0, size = 2)
                    mw0 = int.from_bytes(buffer[0:2],
                                         byteorder = 'big', signed = True)

                except RuntimeError as exc :
                    print("Main : Error reading from PLC :\n", exc)
                    print("Main : Disconnect from PLC")
                    snap7_client.disconnect()
                    PS7_Connect_Done = False

                else:
                    mqtt_client.publish("ps7/s7-315-2dp/db1.dbw0", db1_dbw0)
                    mqtt_client.publish("ps7/s7-315-2dp/ew0", ew0)
                    mqtt_client.publish("ps7/s7-315-2dp/aw0", aw0)
                    mqtt_client.publish("ps7/s7-315-2dp/mw0", mw0)
                    time.sleep(1.05)

    if PS7_Connect_Done:
        print("Main : Disconnect from PLC")
        snap7_client.disconnect()
        PS7_Connect_Done = False

    print("MQTT : Say Bye !")
    mqtt_client.publish("Python-Snap7/Hello", "False")

    print("MQTT : Unsubscribe from topics")
    mqtt_client.unsubscribe(
    [
      "cifX/Hello",
      "a4a/s7-315-2dp/mw0"
    ])

    print("MQTT : Wait a few seconds...")
    time.sleep(5)

    print("MQTT : Stoping loop")
    mqtt_client.loop_stop()

    print("MQTT : Disconnecting from broker")
    mqtt_client.disconnect()

if __name__ == "__main__":
    main()

